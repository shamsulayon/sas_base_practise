
proc import datafile= "/folders/myfolders/Data_082020/score_data_miss777"
DBMS = xlsx out = scoredata0 replace;
run;
proc print data=scoredata0;

data scoredata1;
set scoredata0;
NS1 = score1;
NS2 = score2;
NS3 = score3;

if score1 = 777 THEN
	NS1 = . ;
IF score2 = 777 THEN
	NS2 = .;
IF score3 = 777 THEN
	NS3 = .;
AVERAGE = MEAN(NS1, NS2, NS3);
MAXIMUM = MAX(NS1, NS2, NS3);

data scoredata_subset_if;
set scoredata1;
if NS1 > 80 and NS2 > 80;
run;

data score_data_array;
set scoredata0;
array row_array (3) score1 score2 score3;
do i = 1 to 3;
	if row_array(i)= 777 then row_array(i)=.;
end;
run;
