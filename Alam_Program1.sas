data sdata_blanks;
  infile "/folders/myfolders/DATA_blanks.txt";
/* reading the data */
/*  *C:\Users\shams\OneDrive\Desktop\SasUniversityEdition\myfolders'; */
  input name $ Gender $ Age Weight;
run;

proc print data = sdata_blanks;
run;

/* creating a small data */

data small_data;
input id name $;
cards;
1 ayon
2 rumman
;
run;

proc means data=small_data;
/*var id;*/
run;

/* in order to import messy columns */
data sdata_column;
infile "/folders/myfolders/data_column.txt";
input	name	$	1-5
		gender	$	 6
		weight		7-9
		DOB		$	10 - 19;
run;
proc print data = sdata_column;
run;
/* the above program can also be written as */
data sdata_column;
infile "/folders/myfolders/data_column.txt";
input
@1	name	$	5.
@6	gender	$	1.
@7	weight		3.
@10	DOB		mmddyy10.
;
run;

proc print data = sdata_column;
/*format DOB mmddyy10. */
 format DOB date9.;
run;

/* Coding exercise :1 */
data patient_age;
infile "/folders/myfolders/Patient_HD_age.txt";
input 
@1  pid   1.
@2  Sdate $ mmddyy10.
@12 Edate $ mmddyy10.
@22 age   2.
;
run; 
proc print data = patient_age;
 format Sdate Edate date9.;
run;

/* SAS programming for beginners: 
Section: Getting Data into SAS


Practice
 
1. Create a temporary SAS data 'scoredata0' by Importing an Excel data file score_data.xlsx into SAS (Note: score_data.xlsx is in the downloadable DATA.zip in lecture 2)
2. Create a permanent SAS data set score.scoredata from 'scoredata0'
3. Go to libraries folder in SAS studio, find the data sets in the WORK library
or the SCORE library
4. Both data sets should have 11 observations and 5 variables
*/

proc import datafile = "/folders/myfolders/Data_082020/score_data" 
DBMS = xlsx out = scoredata0 replace ;
run;

LIBNAME score "/folders/myfolders";

data score.scoredata;
set scoredata0;
run;
/*exporting data */
proc export data = score.scoredata (where=(gender = 'f'))
 outfile = '/folders/myfolders/FEMALELIST.csv'
 DBMS = CSV replace;
run;
proc print data = score.scoredata (where=(gender = 'f'));
run;


