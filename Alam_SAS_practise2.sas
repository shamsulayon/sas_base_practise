/* inorder to open is SQL */
proc sql;
create table results.output04 AS
SELECT *
FROM cert.input04 /* where gender in ('m') */
order by VAR1;
quit;


proc print data= cert.input08a;
run;
proc print data= cert.input08b;
run;
proc sql;
create table results.matchtable as
select a.ID,b.ID
from cert.input08a as a
inner join cert.input08b as b /*if we use left join all the a rows will be there */
on a.ID = b.ID;
quit;
proc sql;
create table results.nomatchtable as
select a.ID,b.ID
from cert.input08a as a, cert.input08b as b order by ID;
where a.ID ne b.ID;
quit;

/* usinf sas method to merge */
data results.merge_inputab;
set cert.input08a ; /*it takes the smallest dataset and merges*/
set cert.input08b;
run;

/* Concacting dataset */
data results.concat_inputab;
set set cert.input08a set cert.input08b;
run;
/* Appending data set */
proc append base = cert.input08a;
data = cert.input08b;

/* force option can be used in order to append unmatched variable */
proc append base = cert.input08a
data = cert.input08b force;
run;

/* Concating data by using by statement */
proc sort data = cert.input08a;
by id;
run;

proc sort dara = cert.input08b;
by id;
run;

data results.concat_by;
set cer.input08a cert.input08b;
by id;
run;

data test;
input id name $ gender $age;
datalines;
1 ayon m 28
2 rumman m 26
3 anika f 25
run;
 data test2;
 input id name $ gender $age Cate $;
datalines;
1 maria m 29 A
2 roger f 23 B
3 thomas f 25 C
4 smith m 20 D
run;
data merge_data; /*using keep */
merge test2 test;
by id;
run;

/* mergin data */
data merge_data(keep= id age); /*using keep */
merge test2 test (rename=(name = name1));
by id;
run;

/* so we can see the second data is prioratized and the varibales are replaced */
/*Concating example */
data concat_data;
set test2 test ;
by id;
run;

data One_to_one_merge; /*it takes the smallest data set */
set test ;
set test2 (rename=(name=name1 age = age1)); /*so we can actually retrieve the replaced val*/
run;
proc print data = test;
proc print data = test2;
proc print data = One_to_one_merge;
run;
/* using only the common entities */
data matched_data unmatched_data;
merge test2(in=A) test(IN=B);
if A=1 and B=1 then output matched_data;
else output unmatched_data;
run;

proc print data = test;
proc print data = test2;
proc print data = matched_data;
proc print data = unmatched_data; 


/*if a and not b then output a1;
 if b and not a then output b1;
 if a and b then output c; */