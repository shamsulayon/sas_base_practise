proc import DATAFILE="/folders/myfolders/Data_082020/score_data_miss"
dbms=xlsx out = score_data0 replace;
run;

data score_data1;
set score_data0;
TotalScore = SUM(score1, score2,score3);
Average = Mean(score1, score2, score3);
run;

/*if else statement */
Data ifthenelse;
set score_data1;
if gender = 'm' then gender_num = 1;
else if gender = 'f' then gender_num = 0 ;
else gender_num = .;
run;

/* grade and pass status */
data grade_pass;
set ifthenelse;
if Average >= 90 then do; /* for multiple output statements*/
 grade = 'A';
 pass = 'pass';
 end;
 run;

proc freq data = grade_pass ;
table grade/ missing; */in order to include missing*/
run;

proc freq data = grade_pass ;
tables grade gender*grade/ missing; 
/*in order to include missing and the table
is to create comparision table or the frequesncy table*/
run;

/*Proc univariate */
proc univariate data = grade_pass ;
var Average;
run;

/*using sort data */
proc sort data = grade_pass out = grade_pass_sorted;
by gender;
run;

proc univariate data = grade_pass_sorted ;
var Average;
by gender;
run;
