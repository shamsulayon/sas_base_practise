proc print data=sashelp.class; run;
/**now we will create another data set which is the duplicate of sashelp.**/
data class_duplicate;
     set sashelp.class;
proc print data=class_duplicate;
run;

/** creating data set based on age variable **/
proc sort data=sashelp.class out=ds;
by age;
proc print data=ds;
run;

data class_tmp;
     set ds;
     by age;
run;


/** Take sashelp.class dataset and generate two output datasets one is duplicate and another one is unique
based on the age variable **/
/** hint is first.byvariable name and last.byvariablename **/
data empsalary;
input empid empname $ empdept $ salary;
cards;
101 sudhakar A  75000
102 prit B 50000
103 vishal C 55000
104 surat D 60000
105 tango E 65000
106 alpha A 75000
107 suresh B 50000
108 rima C 55000
109 pooja D 60000
110 tulshi E 65000
111 chailtali A 80000
112 hina A 45000
113 tina B 55000
114 jina B 35000
115 mina C 65000
116 kina C 38000
117 kamina D 65000
118 kutta D 25000
119 riddhi E 89000
120 dharti E 15000
;
run;

proc sort data=empsalary out=empsal;
by empdept descending salary;
run;
data new;
set empsal;
by empdept;
if first.empdept then n=0;
n+1;
if n=2 then output;
drop n;
run;


/** take empsalary dataset and create second highest salary for each department **/
/** hint is first.byvariable name and last.byvariablename **/

data even_ds;
set sashelp.class;
val = _n_;
if mod(val,2)=0 then output;
drop val;
run;
data odd_ds;
set sashelp.class;
val = _n_;
if mod(val,2) ne 0 then output;
run;
/** Take sashelp.class dataset and generate two datasets one is evendataset 
and odd dataset based on the obseravation number **/
/** hint is mod function **/

