/*Use data set ‘scoredata1’ from the coding exercise for ‘data
 preparation’ (for details Please see step 1 and 2 in the coding 
 exercise for ‘data preparation’) */

proc import DATAFILE="/folders/myfolders/Data_082020/score_data_miss"
dbms=xlsx out = score_data0 replace;
run;

data scoredata1;
 set score_data0;
 Average = mean(score1,score2,score3);
run;

proc sort data = scoredata1 out = score_data_sorted;
by gender;
run;

/* Generate frequency tables for all character variables excluding Name*/

proc freq data = scoredata1;
tables score1 score2 score3 Average gender gender*Average;
run;

/*Generate statistic outputs for Ns1,2,3 and Averagecore by Gender using 
Proc Means and Proc Univariate */
proc means data = score_data_sorted n max min range Q1 Q3;
by gender;
var score1 score2 score3 Average;
run;


