libname cert '/folders/myfolders/cert/input/';
libname results '/folders/myfolders/cert/input/';

/*Write a SAS program that will:

Create an output data set results.output12.
Read cert.input12 as input.
Increase the salary variable by 5.65% annually until it is greater than $500,000.
Increment the year variable by 1 with each annual increase.
Create an output data set results.output12 that has one observation for each value of year. 
Each observation should have a year and salary variable.

*/
data results.output12;
  set cert.input12;
  do until (salary gt 500000);
    salary=salary*1.0565;
    year+1;
    output;
  end;
run;

proc print data=results.output12;
run;