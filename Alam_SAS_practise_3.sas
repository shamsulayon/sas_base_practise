libname cert '/folders/myfolders/cert/input/';
libname results '/folders/myfolders/cert/input/';
/* creating a new variable num1 that converts character to numerical
Write a SAS program that will:

Save the new data set as results.output13.
Create a new variable Chdate that converts the date1 variable to a character variable 
that is in the format ddmonyyyy, such as 11NOV1992.
Create a new variable num1 that converts the Charnum variable to a numeric variable.
 */
data results.output13;
set cert.input13;
num1 = input(charnum,  dollar8.);
run;

proc means data=results.output13 mean sum;
run;
/* Create a new variable Chdate that converts the date1 variable to a character variable that is in the format ddmonyyyy, such as 11NOV1992.
Create a new variable num1 that converts the Charnum variable to a numeric variable.*/

/*To convert numerical to character we use the put function */

data results.output13;
set cert.input13;
Chdate = put(date1,  date9.);
format Chdate ddmonyyyy9.;
run;

/*Create output data set results.output27a as a subset of cert.input27 
where the country variable's value is “US” (any variation of case, such as US or us). */

data results.output27a;
set cert.input27;
if Country in ('US', 'us');
run;
proc sort data = results.output27a;
by State descending Postal_Code employee_ID ;
run;
/* Standard code */
proc sort data=cert.input27
out=results.output27a(where=(upcase(country)='US'));
  by state descending Postal_Code employee_ID;
run;

proc print data=results.output27a (firstobs=100 obs=100);
  var employee_ID;
run;
/* removing duplicates Removes duplicate values of Postal_Code, 
keeping only the first occurrence found during the sort.*/
proc sort data = results.output27a nodupkey dupout=results.Duplicate;
by descending Postal_Code ;
run;
/* standard code */
proc sort data=cert.input27 out=results.output27b nodupkey;
  by descending postal_code;
run;

/* proc sort data=DSN noduprecs; */


