/*create a temporary data set, cleandata36.
In this data set, convert all group values to upper case.
Then keep only observations with group equal to 'A' or 'B'.*/

data cleandata36;
set cert.input36;
group = upcase(group);
if group in ('A', 'B');
median_kilogram = median(Kilograms);
run;

/*data cleandata36;
set cert.input36;
group = lowcase(group);
run; */
/*Determine the MEDIAN value for the Kilograms variable for each group (A,B) 
in the cleandata36 data set. Round MEDIAN to the nearest whole number. */

proc sort data= cleandata36;
by group;
run;

proc means data= cleandata36 median;
by group;
run;
/*Ensure that all values for variable Kilograms 
are between 40 and 200, inclusively.If the value is missing or out of range, 
replace the value with the MEDIAN Kilograms value for the respective group (A,B) 
calculated in step 2. */

data results.output36;
set cleandata36;
if Kilograms < 40 or Kilograms > 200 AND group="A" then Kilograms=79;
else if Kilograms < 40 or Kilograms > 200 AND group="B" then Kilograms=89;
run;
proc means data = results.output36;
by group;
run;

/*standard code */
data results.output36;
  set cleandata36;
  if Kilograms < 40 or Kilograms > 200 then do;
    if group='A' then kilograms=79;
    else kilograms=89;
  end;
run;